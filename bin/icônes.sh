#!/usr/bin/env bash
set -eu

: "${POWERLINE_ICONS=nerd-fonts}"

if ! [ -v __powerline_context ] ; then
	# shellcheck source=/dev/null
	. powerline.bash
fi

lister_icones() {
	printf "%s\n" "${!__powerline_icons[@]}"
}

mapfile -t icones < <(lister_icones | sort)

echo "${POWERLINE_ICONS@A}"
echo

for i in "${icones[@]}" ; do
	printf "%-16s %s texte\n" "$i" "${__powerline_icons[$i]}"
done
