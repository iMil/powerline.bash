#!/usr/bin/env bash
#
# Affiche la palette de couleur.
#
set -eu

main() {
	local couleurs

	if ! [ -v __powerline_context ] ; then
		# shellcheck source=/dev/null
		. powerline.bash
	fi

	filtre_alias='(-fond|-texte|-icone|-erreur|-succ|commande)'

	echo "Couleurs"
	echo

	mapfile -t couleurs < <(iter_couleurs | grep -Ev "$filtre_alias")
	afficher "${couleurs[@]}"

	echo
	echo "Alias"
	echo

	mapfile -t couleurs < <(iter_couleurs | grep -E "$filtre_alias")
	afficher "${couleurs[@]}"
}

iter_couleurs() {
	printf '%s\n' "${!__powerline_colors[@]}" | sort
}

afficher() {
	for couleur in "${couleurs[@]}" ; do
		fond="${__powerline_colors[$couleur]}"
		texte="${fond/48;/38;}"
		printf "\e[%sm        \e[0m\e[0;%sm\uE0B0 " "$fond" "$texte"
		printf "\e[0;%sm texte \e[0m\uE0B1 " "$texte"
		printf "\e[0m    %s\n" "$couleur"
	done
}

main
