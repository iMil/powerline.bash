#!/usr/bin/env bash
#
# Affiche la palette des 256 couleurs RGB
#
set -eu

afficher_couleur() {
	local couleur="$1"
        printf -v texte "%7s" "48;5;${2-$couleur}"
        echo -en "\\e[48;5;${couleur};38;5;0m ${texte} \\e[38;5;15m ${texte} "
}

for couleur in {0..15} ; do
	afficher_couleur "$couleur"
        if [ "$(( (1 + couleur) % 8))" -eq 0 ] ; then
                echo -e "\\e[0m"
        fi 
done
